package main

import (
	"fmt"
	"net"
	"os"
	"bufio"
)

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err.Error())
		os.Exit(1)
	}
}

func readInput(out chan []byte, uname string) {
	var msg string
	prompt := uname + ": "
	instream := bufio.NewReader(os.Stdin)
	for {
		fmt.Printf("%s", prompt)
		input, err := instream.ReadString('\n')
		if err != nil {
			continue
		}
		msg = "\n" + prompt + input
		out <- []byte(msg)
	}
}

func printOutput(in chan []byte) {
	var msg []byte
	for {
		msg = <-in
		fmt.Println(string(msg))
	}
}

func recMsg(conn net.Conn, in chan []byte) {
	buf := make([]byte, 1024)
	for {
		_, err := conn.Read(buf[0:])
		if err != nil {
			continue
		}

		in <- buf
	}
}

func sendMsg(conn net.Conn, out chan []byte) {
	for {
		msg := <-out
		_, err := conn.Write(msg)
		if err != nil {
			continue
		}
	}
}

func main() {

	if len(os.Args) != 3 {
		fmt.Println("Usage: clschat username ip:port")
		os.Exit(1)
	}	
	uname := os.Args[1]
	target := os.Args[2]

	server, err := net.ResolveTCPAddr("tcp", target)
	checkError(err)

	conn, err := net.DialTCP("tcp", nil, server)
	checkError(err)

	out := make(chan []byte)
	in := make(chan []byte)

	go printOutput(in)
	go recMsg(conn, in)
	go sendMsg(conn, out)
	readInput(out, uname)
}