package main

import (
	"fmt"
	"net"
	"os"
)


var clients []net.Conn

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err.Error())
		os.Exit(1)
	}
}

func handleClient(conn net.Conn, mesg chan []byte, ind chan int) {
	defer conn.Close()
	fmt.Println("Client connected")
	buf := make([]byte, 1024)
	for {

		n, err := conn.Read(buf[0:])
		if err != nil {
			continue
		}
		fmt.Println(string(buf[0:n]))
		mesg <- buf[0:n]
		fmt.Println(mesg)
		ind <- n 
		fmt.Println(ind)

	}
}

func sendMessages(mesg chan []byte, ind chan int) {
	fmt.Println("Message Sender Ready")

	for {
		buf := <-mesg
		n := <-ind
		fmt.Println("Channels recieved")

		for _, k := range clients {
			_, err := k.Write(buf[0:n])
			if err != nil {
				continue
			}
		}
	}
}

func main() {
	port := ":1201"
	tcpAddr, err := net.ResolveTCPAddr("tcp", port)
	checkError(err)

	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	messages := make(chan []byte)
	index := make(chan int)

	go sendMessages(messages, index)

	for {
		conn, err := listener.Accept()
		checkError(err)

		go handleClient(conn, messages, index)
		clients = append(clients, conn)
	}
}